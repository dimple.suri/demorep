package dao;
import java.util.*;

import model.Product;
public class ProductDAO {
	private List<Product> plist;
	public ProductDAO()
	{
		plist=new ArrayList<Product>();
		
	}
	public void insertProduct(Product p)
	{
		plist.add(p);
	}
	public List<Product> getData()
	{
		return plist;
	}
	public void deleteProduct(int pid) {
		Product pdelete=null;
		for(Product p1:plist)
		{
			if(p1.getProductid()==pid)
			{
				pdelete=p1;
			}
		}
		plist.remove(pdelete);
		
	}
	

}
