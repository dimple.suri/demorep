package service;
import java.util.*;

import dao.ProductDAO;
import model.Product;
public class ProductServiceImpl implements ProductService{
private Scanner sc;
private ProductDAO pdao;
public ProductServiceImpl()
{
	sc=new Scanner(System.in);
	pdao=new ProductDAO();
}
	@Override
	public void insert() {
		System.out.println("Enter number of products u want to enter");
		int noofproducts=sc.nextInt();
		for(int x=1;x<=noofproducts;x++)
		{
			Product p=new Product();
			System.out.println("Enter Product id ");
			p.setProductid(sc.nextInt());
			System.out.println("Enter Product name ");
			p.setProductname(sc.next());
			System.out.println("Enter Price ");
			p.setPrice(sc.nextDouble());
			pdao.insertProduct(p);
			
		}
		
	}

	@Override
	public void delete() {
		System.out.println("Enter product id which u want to delete");
		pdao.deleteProduct(sc.nextInt());
		
	}

	@Override
	public void retreive() {
		List<Product> productlist=pdao.getData();
		for(Product p:productlist)
		{
			System.out.println("Product name is "+p.getProductname());
			System.out.println("Price is "+p.getPrice());
		}
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
